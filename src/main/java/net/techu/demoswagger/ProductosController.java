package net.techu.demoswagger;

import com.sun.source.tree.ReturnTree;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.techu.data.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {

    private ArrayList<Producto> listaProductos;

    public ProductosController() {
        listaProductos = new ArrayList<Producto>();
        listaProductos.add(new Producto("ABC", "Producto ABC", 123.22, "CAT1"));
        listaProductos.add(new Producto("DEF", "Producto DEF", 321.99, "CAT2"));
        listaProductos.add(new Producto("GHI", "Producto GHI", 444.73, "CAT3"));
    }

    @GetMapping("/productos")
    public ResponseEntity<ArrayList<Producto>> getListaProductos() {
        return new ResponseEntity<ArrayList<Producto>>(listaProductos, HttpStatus.OK);
    }

    @PostMapping("/productos")
    @ApiOperation(value="Crear producto", notes="Este método crea un producto")
    public ResponseEntity<Producto> addProducto(@ApiParam(name="producto",
                                                            type="Producto",
                                                            value="producto a crear",
                                                            example="PR1",
                                                           required = true) @RequestBody Producto newProducto) {
        listaProductos.add(newProducto);
        return new ResponseEntity<Producto>(newProducto, HttpStatus.OK);
    }

    @PostMapping("/productos/byname")
    @ApiOperation(value="Crear producto por nombre", notes="Este método crea un producto solo con el nombre")
    public ResponseEntity<Producto> addProductoByName(@ApiParam(name="producto",
            type="String",
            value="nombre del producto a crear",
            example="PR1",
            required = true) @RequestParam String nombre) {
        Producto productoNuevo = new Producto("", nombre, 99, "CAT5");
        listaProductos.add(productoNuevo);
        return new ResponseEntity<Producto>(productoNuevo, HttpStatus.OK);
    }

}
